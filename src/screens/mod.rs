use sdl2::{event::Event, rect::Rect, surface::Surface};

use crate::graphics::{
    geometry::*,
    render::Renderer,
    sprite::{sheet::Spritesheet, *},
    Texture, TextureCreator,
};

mod levels;
pub mod main_menu;
pub mod window_focus;

pub enum SceneTransition {
    Pop,
    Push(Box<dyn Scene>),
    Continue,
}

pub trait Scene {
    fn tick(&mut self, event: Event) -> SceneTransition;

    fn render(&self, screen: &Renderer);
}

// TODO: https://en.wikipedia.org/wiki/9-slice_scaling
pub fn get_ui_window(
    texture_creator: &'static TextureCreator,
    spritesheet: &'static Spritesheet,
    w: usize,
    h: usize,
) -> Texture {
    use FaceDirection::*;
    use Sprite::MenuEdge;

    assert!(w > 1 && h > 1, "UI Window must be greater than or equal to (2, 2)");

    let mut ui_window = Surface::new_sprite((w as i32, h as i32));

    for yy in 0..h {
        for xx in 0..w {
            let sprite = {
                MenuEdge(if (xx, yy) == (0, 0) {
                    TopLeft
                } else if (xx, yy) == (w - 1, 0) {
                    TopRight
                } else if (xx, yy) == (0, h - 1) {
                    BottomLeft
                } else if (xx, yy) == (w - 1, h - 1) {
                    BottomRight
                } else if yy == 0 {
                    Top
                } else if yy == h - 1 {
                    Bottom
                } else if xx == 0 {
                    Left
                } else if xx == w - 1 {
                    Right
                } else {
                    Center
                })
            };
            let dst = Rect::new_tile(xx as i32, yy as i32, 1, 1);
            spritesheet[&sprite]
                .blit(None, &mut ui_window, dst)
                .unwrap_or_else(|e| {
                    panic!(
                        "unable to copy sprite to UI window (sprite={sprite:?}) at {:?}: {e}",
                        dst.top_left()
                    )
                });
        }
    }

    ui_window.set_colors(
        None,
        RGB(0x0B, 0x0B, 0x23),
        RGB(0x16, 0x16, 0x89),
        RGB(0xC3, 0xC3, 0xDB),
    );

    ui_window.to_texture(texture_creator)
}
