use std::cell::OnceCell;

use sdl2::{event::Event, keyboard::Keycode, rect::Point};

use crate::{
    graphics::{noise::create_noise, render::Renderer, sprite::sheet::Spritesheet, Texture, TextureCreator},
    screens::{Scene, SceneTransition},
};

pub struct TestGame {
    _ss: &'static Spritesheet,
    _tc: &'static TextureCreator,
    texture: OnceCell<Texture>,
}
impl TestGame {
    pub fn new(ss: &'static Spritesheet, tc: &'static TextureCreator) -> Self {
        Self {
            _ss: ss,
            _tc: tc,
            texture: OnceCell::new(),
        }
    }
}
impl Scene for TestGame {
    fn tick(&mut self, event: Event) -> SceneTransition {
        match event {
            Event::Quit { .. }
            | Event::KeyDown {
                keycode: Some(Keycode::Escape | Keycode::Q),
                ..
            } => return SceneTransition::Pop,
            _ => (),
        }
        SceneTransition::Continue
    }

    fn render(&self, screen: &Renderer) {
        let (width, height) = screen.shape();
        let texture = self
            .texture
            .get_or_init(|| create_noise(self._tc, width as _, height as _));

        screen.render_texture(texture, Point::new(0, 0));
    }
}
