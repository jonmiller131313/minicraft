use sdl2::{event::Event, keyboard::Keycode, rect::Point};

use super::{levels::test::TestGame, Scene, SceneTransition};
use crate::{
    graphics::{
        geometry::PointTileExt,
        menu::CenteredMenu,
        render::Renderer,
        sprite::{sheet::Spritesheet, *},
        Texture, TextureCreator,
    },
    sprite_text,
};

pub struct MainMenu {
    tc: &'static TextureCreator,
    ss: &'static Spritesheet,
    version: Texture,
    title: Texture,
    menu: CenteredMenu,
}
impl MainMenu {
    const START_INDEX: usize = 0;
    const CONTROLS_INDEX: usize = 1;
    const ABOUT_INDEX: usize = 2;
    const EXIT_INDEX: usize = 3;

    pub fn new(tc: &'static TextureCreator, ss: &'static Spritesheet) -> Self {
        let mut title = ss[&Sprite::Title].clone_sprite(None);
        title.set_colors(
            None,
            RGB(0x1C, 0x2E, 0x42),
            RGB(0x4A, 0x78, 0x4D),
            RGB(0xE1, 0xE4, 0x84),
        );

        let mut version = sprite_text!(ss, tc, "v{}", env!("CARGO_PKG_VERSION"));
        version.as_mut().set_color_mod(0x38, 0x38, 0x38);
        Self {
            tc,
            ss,
            version,
            title: title.to_texture(&tc),
            menu: CenteredMenu::new(ss, tc, None, 1, ["start game", "controls", "about", "exit"]),
        }
    }
}
impl Scene for MainMenu {
    fn tick(&mut self, event: Event) -> SceneTransition {
        match event {
            Event::Quit { .. } => SceneTransition::Pop,
            Event::KeyDown {
                keycode: Some(Keycode::Up),
                ..
            } => {
                self.menu.up();
                SceneTransition::Continue
            }
            Event::KeyDown {
                keycode: Some(Keycode::Down),
                ..
            } => {
                self.menu.down();
                SceneTransition::Continue
            }
            Event::KeyDown {
                keycode: Some(Keycode::Return),
                ..
            } => match self.menu.current_selection() {
                Self::START_INDEX => SceneTransition::Push(Box::new(TestGame::new(self.ss, self.tc))),
                Self::CONTROLS_INDEX => SceneTransition::Push(Box::new(ControlScreen::new(self.ss, self.tc))),
                Self::ABOUT_INDEX => SceneTransition::Push(Box::new(AboutScreen::new(self.ss, self.tc))),
                Self::EXIT_INDEX => SceneTransition::Pop,
                _ => unreachable!(),
            },
            _ => SceneTransition::Continue,
        }
    }

    fn render(&self, screen: &Renderer) {
        screen.fill_screen(RGB(0x0A, 0x0A, 0x0A));
        screen.render_texture_centered(&self.title, 3);
        screen.render_texture_right(&self.version, screen.tile_shape().1 - 1);
        self.menu.render(screen, 8);
    }
}

pub struct ControlScreen {
    title: Texture,
    lines: Vec<Texture>,
}
impl ControlScreen {
    fn new(ss: &Spritesheet, tc: &'static TextureCreator) -> Self {
        let mut title = ss.render_str(tc, "controls");
        title.as_mut().set_color_mod(0x38, 0x38, 0x38);

        let lines = [
            "walk: wasd",
            "menu navigation: arrow keys",
            "attack / break: space",
            "inventory / crafting: e",
            "place / interact: space",
            "pause: esc",
        ]
        .into_iter()
        .map(|line| {
            let mut line = ss.render_str(tc, line);
            line.as_mut().set_color_mod(0x94, 0x94, 0x94);
            line
        })
        .collect();

        Self { title, lines }
    }
}
impl Scene for ControlScreen {
    fn tick(&mut self, event: Event) -> SceneTransition {
        match event {
            Event::KeyDown {
                keycode: Some(Keycode::Escape),
                ..
            } => SceneTransition::Pop,
            _ => SceneTransition::Continue,
        }
    }

    fn render(&self, screen: &Renderer) {
        screen.fill_screen(RGB(0x0A, 0x0A, 0x0A));
        screen.render_texture_centered(&self.title, 0);

        for (i, line) in self.lines.iter().enumerate() {
            screen.render_texture_centered(line, i + 2);
        }
    }
}

pub struct AboutScreen {
    title: Texture,
    about: Texture,
}
impl AboutScreen {
    fn new(ss: &Spritesheet, tc: &'static TextureCreator) -> Self {
        let about = r"
minicraft was made
by markus persson
for the 22'nd ludum
dare competition in
december 2011.

it is dedicated to
my father. <3

ported and modified
by jonathan miller.
";
        let mut about = ss.render_paragraph(tc, about);
        about.as_mut().set_color_mod(0x94, 0x94, 0x94);
        Self {
            title: ss.render_str(tc, "about minicraft"),
            about,
        }
    }
}
impl Scene for AboutScreen {
    fn tick(&mut self, event: Event) -> SceneTransition {
        match event {
            Event::KeyDown {
                keycode: Some(Keycode::Escape),
                ..
            } => SceneTransition::Pop,
            _ => SceneTransition::Continue,
        }
    }

    fn render(&self, screen: &Renderer) {
        screen.fill_screen(RGB(0x0A, 0x0A, 0x0A));
        screen.render_texture(&self.title, Point::new_tile(2, 1) + Point::new(4, 0));
        screen.render_texture(&self.about, Point::new_tile(0, 2) + Point::new(4, 0));
    }
}
