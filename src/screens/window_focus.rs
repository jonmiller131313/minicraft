use std::{
    cell::{Cell, RefCell},
    time::{Duration, Instant},
};

use sdl2::{
    event::{Event, WindowEvent::FocusGained},
    pixels::Color,
};

use super::{get_ui_window, Scene, SceneTransition};
use crate::graphics::{
    render::Renderer,
    sprite::{sheet::Spritesheet, *},
    Texture, TextureCreator,
};

pub struct FocusLost {
    _tc: &'static TextureCreator,
    _ss: &'static Spritesheet,
    window: Texture,
    text: RefCell<Texture>,
    color_index: Cell<usize>,
    next_color_toggle: Cell<Instant>,
}
impl FocusLost {
    const TEXT_COLORS: [Color; 2] = [RGB(0xF0, 0xF0, 0xF0), RGB(0x94, 0x94, 0x94)];

    pub fn new(tc: &'static TextureCreator, ss: &'static Spritesheet) -> Self {
        let msg = "click to focus!";
        Self {
            _tc: tc,
            _ss: ss,
            window: get_ui_window(tc, ss, msg.len() + 2, 3),
            text: RefCell::new(ss.render_str(tc, msg)),
            color_index: Cell::new(0),
            next_color_toggle: Cell::new(Instant::now()),
        }
    }

    fn check_color_switch(&self) {
        if Instant::now() > self.next_color_toggle.get() {
            let new_index = self.color_index.get() ^ 1;
            self.color_index.set(new_index);
            let Color { r, g, b, .. } = Self::TEXT_COLORS[new_index];
            self.text.borrow_mut().as_mut().set_color_mod(r, g, b);
            self.next_color_toggle.set(Instant::now() + Duration::from_millis(333));
        }
    }
}
impl Scene for FocusLost {
    fn tick(&mut self, event: Event) -> SceneTransition {
        self.check_color_switch();
        if let Event::Window {
            win_event: FocusGained, ..
        } = event
        {
            SceneTransition::Pop
        } else {
            SceneTransition::Continue
        }
    }

    fn render(&self, screen: &Renderer) {
        self.check_color_switch();
        screen.render_texture_centered(&self.window, 13);
        screen.render_texture_centered(&self.text.borrow(), 14);
    }
}
