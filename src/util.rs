use std::fmt::Display;

use anyhow::{Context, Error, Result};
use directories::BaseDirs;
use once_cell::sync::Lazy;

pub static BASE_DIRS: Lazy<BaseDirs> = Lazy::new(|| BaseDirs::new().expect("unable to get base directories?"));

/// An extension trait for most of SDL2's error handling, which sadly is mostly `Result<_, String>`, to allow
/// functions from the [Context] trait in the [anyhow] crate.
///
/// This is needed due to [String] not implementing the [Error] trait, the error messages get wrapped via
/// [`Error::msg`].
///
/// See [issue `#1053`](https://github.com/Rust-SDL2/rust-sdl2/issues/1053) in the `sdl2` crate for any developments.
pub trait SdlContextExt<T, E> {
    /// Functionally equivalent to [`Context::context()`].
    fn context<C>(self, context: C) -> Result<T>
    where
        C: Display + Send + Sync + 'static;

    /// Functionally equivalent to [`Context::with_context()`].
    fn with_context<C, F>(self, f: F) -> Result<T>
    where
        C: Display + Send + Sync + 'static,
        F: FnOnce() -> C;
}

impl<T> SdlContextExt<T, String> for Result<T, String> {
    fn context<C>(self, context: C) -> Result<T>
    where
        C: Display + Send + Sync + 'static,
    {
        self.map_err(Error::msg).context(context)
    }

    fn with_context<C, F>(self, f: F) -> Result<T>
    where
        C: Display + Send + Sync + 'static,
        F: FnOnce() -> C,
    {
        self.map_err(Error::msg).with_context(f)
    }
}
