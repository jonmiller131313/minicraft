use std::{
    env,
    time::{Instant, SystemTime, UNIX_EPOCH},
};

use itertools::Itertools;
use noise::{NoiseFn, Value};
use rayon::prelude::*;
use sdl2::surface::Surface;

use crate::graphics::{
    sprite::{TileSpriteExt, RGB},
    Texture, TextureCreator,
};

pub fn create_noise(tc: &'static TextureCreator, width: usize, height: usize) -> Texture {
    let start = Instant::now();

    let noise = Value::new(get_seed());

    let mut surface = Surface::new_sprite((width as _, height as _));
    let format = surface.pixel_format();

    let mut pixels = surface.pixels_mut();

    let height_map = (0..height)
        .into_par_iter()
        .map(|y| get_row_noise(&noise, y, width))
        .collect::<Vec<_>>()
        .into_iter()
        .sorted_unstable_by_key(|(y, _)| *y)
        .map(|(_, noise)| noise)
        .collect_vec();

    for y in 0..height {
        let row = &mut pixels[y];
        for x in 0..width {
            let greyscale = ((height_map[y][x]) * 256.) as _;
            row[x] = RGB(greyscale, greyscale, greyscale).to_u32(&format);
        }
    }

    let end = Instant::now();
    println!("Took {:.3?} to generate", end - start);

    surface.to_texture(tc)
}

fn get_row_noise(noise: &Value, y: usize, width: usize) -> (usize, Vec<f64>) {
    let mut row = Vec::with_capacity(width);
    for x in 0..width {
        let nx = x as f64 / width as f64 - 0.5;
        let ny = y as f64 / width as f64 - 0.5;

        let num_octaves = 5 - 1;
        let mut amplitude = 1.;
        let mut frequency = 4.;
        let mut scale_sum = amplitude;

        let mut n = (noise.get([frequency * nx, frequency * ny]) + 1.) / 2.;
        for _ in 0..num_octaves {
            amplitude /= 2.;
            frequency *= 2.;
            scale_sum += amplitude;
            n += amplitude * (noise.get([frequency * nx + frequency, frequency * ny + frequency]) + 1.) / 2.;
        }
        row.push((n / scale_sum).powi(2));
    }
    (y, row)
}

fn get_seed() -> u32 {
    let seed = env::var("RNG_SEED")
        .map(|seed| seed.parse().expect("RNG seed must be a number"))
        .unwrap_or_else(|_| SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_micros() as _);
    eprintln!("Seed: {seed}");
    seed
}
