use sdl2::rect::Rect;

use super::{SpritesheetLoad, SPRITEMAP_TILE_SIZE};
use crate::graphics::{geometry::*, sprite::*};

impl SpritesheetLoad for Sprite {
    fn load(&self, spritesheet: &Surface) -> Surface {
        use FaceDirection::*;
        use Material::*;
        use Sprite::*;
        use SpriteColor::*;
        let (x, y, w, h, a) = match self {
            Ground1 => (0, 0, 1, 1, DarkGrey),
            Ground2 => (1, 0, 1, 1, DarkGrey),
            Ground3 => (2, 0, 1, 1, DarkGrey),
            Ground4 => (3, 0, 1, 1, DarkGrey),
            Stone => (0, 1, 1, 1, DarkGrey),
            Flower => (1, 1, 1, 1, DarkGrey),
            Footprint => (3, 1, 1, 1, DarkGrey),
            Edge(MaterialFace { facing, material }) => {
                let y = match facing {
                    TopLeft | Top | TopRight => 0,
                    Left | Center | Right => 1,
                    BottomLeft | Bottom | BottomRight => 2,
                };
                let x = match facing {
                    TopLeft | Left | BottomLeft => 0,
                    Top | Center | Bottom => 1,
                    TopRight | Right | BottomRight => 2,
                } + match material {
                    StoneWall => 4,
                    Grass => 11,
                    Ground => 14,
                };
                (x, y, 1, 1, White)
            }
            TreeSingle => {
                let panic_err = |e| panic!("unable to construct tree sprite: {e}");
                let mut sprite = Surface::new_sprite((2, 2));
                spritesheet
                    .blit(Rect::new_tile(9, 0, 2, 2), &mut sprite, None)
                    .unwrap_or_else(panic_err);
                spritesheet
                    .blit(Rect::new_tile(10, 3, 1, 1), &mut sprite, Rect::new_tile(1, 1, 1, 1))
                    .unwrap_or_else(panic_err);
                sprite.set_color(White.into(), TRANSPARENT);
                return sprite;
            }
            TreeCluster => {
                // TODO
                return TreeSingle.load(spritesheet);
            }
            Sapling => (11, 3, 1, 1, White),
            Ore => (17, 1, 2, 2, Black),
            StepsDown => (0, 2, 2, 2, Black),
            StepsUp => (2, 2, 2, 2, Black),
            Crop(None) => (2, 1, 1, 1, DarkGrey),
            Crop(Some(CropGrowthStage(i))) => (4 + *i as i32, 3, 1, 1, DarkGrey),
            Cactus => (8, 2, 2, 2, White),
            MenuEdge(direction) => return render_menu_sprite(spritesheet, *direction),
            Title => (0, 6, 13, 2, Black),
            Character(ch) => {
                let mut x = ch.position() as i32;
                let mut y = SPRITEMAP_TILE_SIZE as i32 - 2;
                if x > 26 {
                    x -= 27;
                    y += 1;
                }
                (x, y, 1, 1, Black)
            }
            Chest => (2, 8, 2, 2, Black),
        };
        let mut sprite = spritesheet.clone_sprite(Rect::new_tile(x, y, w, h));
        sprite.set_color(a.into(), TRANSPARENT);

        sprite
    }
}

fn render_menu_sprite(spritesheet: &Surface, direction: FaceDirection) -> Surface {
    use FaceDirection::*;
    use SpriteColor::Black;
    if direction == Center {
        let mut sprite = Surface::new_sprite((1, 1));
        sprite
            .fill_rect(None, SpriteColor::LightGrey.into())
            .expect("unable to fill center menu color");
        return sprite;
    }
    let y = 13;
    let x = match direction {
        Top | Bottom => 1,
        Left | Right => 2,
        _ => 0,
    };
    let mut sprite = spritesheet.clone_sprite(Rect::new_tile(x, y, 1, 1));
    let (flip_h, flip_v) = match direction {
        TopRight | Right => (true, false),
        BottomRight => (true, true),
        BottomLeft | Bottom => (false, true),
        _ => (false, false),
    };

    if flip_h {
        sprite.flip_horizontally();
    }
    if flip_v {
        sprite.flip_vertically();
    }

    sprite.set_color(Black.into(), TRANSPARENT);

    sprite
}
