use std::{collections::HashMap, ops::Index};

use anyhow::{ensure, Result};
use sdl2::{image::LoadSurface, pixels::PixelFormatEnum::RGBA8888, rect::Rect};

use super::{Sprite, SpriteChar, Surface, TileSpriteExt, RGB};
use crate::{
    graphics::{geometry::*, sprite::Sprite::Chest, Texture, TextureCreator},
    util::*,
};

mod load;

const SPRITEMAP_TILE_SIZE: usize = 32;

pub struct Spritesheet(HashMap<Sprite, Surface>);
impl Spritesheet {
    pub fn load() -> Result<Spritesheet> {
        let spritesheet_path = BASE_DIRS.data_dir().join("assets/sprites.png");

        let sheet_surface = Surface::from_file(&spritesheet_path)
            .with_context(|| format!("unable to load sprite from file: {spritesheet_path:?}"))?
            .convert_format(RGBA8888)
            .context("unable to convert sprite")?;
        let loaded_sheet_size = sheet_surface.size();
        let expected_sheet_size = (
            (SPRITE_SIZE * SPRITEMAP_TILE_SIZE) as u32,
            (SPRITE_SIZE * SPRITEMAP_TILE_SIZE) as u32,
        );
        ensure!(
            loaded_sheet_size == expected_sheet_size,
            "Loaded sheet size {loaded_sheet_size:?} not supported size {expected_sheet_size:?}"
        );

        let mut spritesheet: HashMap<Sprite, Surface> = HashMap::with_capacity(SPRITEMAP_TILE_SIZE * 3);
        for sprite in enum_iterator::all() {
            spritesheet.insert(sprite, sprite.load(&sheet_surface));
        }

        Ok(Self(spritesheet))
    }

    pub fn render_str(&self, texture_creator: &'static TextureCreator, msg: &str) -> Texture {
        let mut tgt = Surface::new_sprite((msg.len() as _, 1));
        msg.chars()
            .map(|ch| &self.0[&Sprite::Character(SpriteChar::from(ch))])
            .enumerate()
            .map(|(i, sp)| (Rect::new_tile(i as _, 0, 1, 1), sp))
            .for_each(|(dst, ch)| {
                ch.blit(None, &mut tgt, dst).expect("unable to copy letter to msg");
            });

        tgt.to_texture(texture_creator)
    }

    pub fn render_paragraph(&self, texture_creator: &'static TextureCreator, msg: &str) -> Texture {
        let lines: Vec<_> = msg.lines().collect();
        let widest = lines.iter().copied().map(str::len).max().expect("must pass text");
        let mut tgt = Surface::new_sprite((widest as _, lines.len() as _));
        lines.into_iter().enumerate().for_each(|(row, line)| {
            line.chars()
                .map(|ch| &self.0[&Sprite::Character(SpriteChar::from(ch))])
                .enumerate()
                .map(|(i, sp)| (Rect::new_tile(i as _, row as _, 1, 1), sp))
                .for_each(|(dst, ch)| {
                    ch.blit(None, &mut tgt, dst).expect("unable to copy letter to msg");
                })
        });
        tgt.to_texture(texture_creator)
    }

    pub fn make_window_icon(&self) -> Surface {
        let mut icon = Surface::new(512, 410, RGBA8888).unwrap();

        let mut chest = self.0[&Chest].clone_sprite(Rect::new(2, 5, 13, 10));
        chest.set_colors(
            None,
            RGB(0x35, 0x35, 0x1D),
            RGB(0x8e, 0x8e, 0x60),
            RGB(0xE7, 0xE7, 0xA3),
        );

        chest
            .blit_scaled(None, &mut icon, Rect::new(34, 34, 444, 342))
            .expect("unable to upscale game icon");

        icon
    }
}
impl Index<&Sprite> for Spritesheet {
    type Output = Surface;

    fn index(&self, index: &Sprite) -> &Self::Output {
        &self.0[index]
    }
}

trait SpritesheetLoad {
    fn load(&self, spritesheet: &Surface) -> Surface;
}
