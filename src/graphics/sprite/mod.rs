//! Module handling pixel-based graphics
use std::{
    fmt,
    mem::size_of,
    ops::{Index, IndexMut, Range},
};

use bytemuck::{cast_slice, cast_slice_mut};
use const_str::to_char_array;
use enum_iterator::Sequence;
use sdl2::{
    pixels::{Color, PixelFormatEnum::RGBA8888},
    rect::{Point, Rect},
    surface::Surface as SdlSurface,
};

use self::sheet::Spritesheet;
use super::geometry::*;
use crate::graphics::{Texture, TextureCreator};

pub mod sheet;

pub type Surface = SdlSurface<'static>;

#[allow(non_snake_case)]
pub const fn RGB(r: u8, g: u8, b: u8) -> Color {
    Color::RGB(r, g, b)
}

#[allow(non_snake_case)]
pub const fn RGBA(r: u8, g: u8, b: u8, a: u8) -> Color {
    Color::RGBA(r, g, b, a)
}

pub const TRANSPARENT: Color = RGBA(0xFF, 0xFF, 0xFF, 0x00);
const DARK_GREY: Color = RGB(0x51, 0x51, 0x51);
const LIGHT_GREY: Color = RGB(0xAD, 0xAD, 0xAD);

/// The four colors present in the sprite.
#[allow(missing_docs)]
pub enum SpriteColor {
    Black,
    DarkGrey,
    LightGrey,
    White,
}
impl From<&SpriteColor> for Color {
    fn from(color: &SpriteColor) -> Self {
        use SpriteColor::*;
        match color {
            Black => Color::BLACK,
            White => Color::WHITE,
            DarkGrey => DARK_GREY,
            LightGrey => LIGHT_GREY,
        }
    }
}
impl From<SpriteColor> for Color {
    fn from(color: SpriteColor) -> Self {
        Self::from(&color)
    }
}

pub trait TileSpriteExt {
    fn new_sprite(shape: impl Into<Point>) -> Self;

    fn clone_sprite(&self, src: impl Into<Option<Rect>>) -> Self;

    fn set_color(&mut self, before: Color, after: Color);

    fn set_colors(
        &mut self,
        color1: impl Into<Option<Color>>,
        color2: impl Into<Option<Color>>,
        color3: impl Into<Option<Color>>,
        color4: impl Into<Option<Color>>,
    );

    fn flip_vertically(&mut self);

    fn flip_horizontally(&mut self);

    fn to_texture(&self, texture_creator: &'static TextureCreator) -> Texture;

    fn pixels_mut(&mut self) -> SpritePixels;
}
impl TileSpriteExt for Surface {
    fn new_sprite(shape: impl Into<Point>) -> Self {
        let (w, h) = shape.into().into();
        Self::new((w * SPRITE_SIZE as i32) as _, (h * SPRITE_SIZE as i32) as _, RGBA8888)
            .unwrap_or_else(|e| panic!("unable to create tile sprite with shape {w}x{h}: {e}"))
    }

    fn clone_sprite(&self, src: impl Into<Option<Rect>>) -> Self {
        let src = src.into();
        let (w, h) = src.unwrap_or_else(|| self.rect()).size();
        let mut sprite = Self::new(w, h, RGBA8888)
            .unwrap_or_else(|e| panic!("unable to create tile sprite with shape ({w},{h}): {e}"));
        self.blit(src, &mut sprite, None).unwrap_or_else(|e| {
            let _loc = src.unwrap_or_else(|| self.rect()).top_left();
            let _size = (w, h);
            panic!("unable to create a sprite with location {_loc:?} and size {_size:?}: {e}")
        });

        sprite
    }

    fn set_color(&mut self, before: Color, after: Color) {
        let h = self.size().1 as _;
        let format = self.pixel_format();
        let before = before.to_u32(&format);
        let after = after.to_u32(&format);
        let mut pixels = self.pixels_mut();
        for i in 0..h {
            for pixel in &mut pixels[i] {
                if *pixel == before {
                    *pixel = after;
                }
            }
        }
    }

    fn set_colors(
        &mut self,
        color1: impl Into<Option<Color>>,
        color2: impl Into<Option<Color>>,
        color3: impl Into<Option<Color>>,
        color4: impl Into<Option<Color>>,
    ) {
        let (c1, c2, c3, c4) = (color1.into(), color2.into(), color3.into(), color4.into());
        let format = self.pixel_format();
        let h = self.size().1 as _;
        let mut pixels = self.pixels_mut();
        for i in 0..h {
            for pixel in &mut pixels[i] {
                let p_color = Color::from_u32(&format, *pixel);
                let mut set_color = |c: Color| *pixel = c.to_u32(&format);
                match (p_color, c1, c2, c3, c4) {
                    (Color::BLACK, Some(new_c), ..) => set_color(new_c),
                    (DARK_GREY, _, Some(new_c), ..) => set_color(new_c),
                    (LIGHT_GREY, .., Some(new_c), _) => set_color(new_c),
                    (Color::WHITE, .., Some(new_c)) => set_color(new_c),
                    _ => (),
                }
            }
        }
    }

    fn flip_vertically(&mut self) {
        let h = self.size().1;
        let mut pixels = self.pixels_mut();

        for top_i in 0..(h as usize / 2) {
            pixels.swap_rows(top_i, h as usize - 1 - top_i);
        }
    }

    fn flip_horizontally(&mut self) {
        let h = self.size().1 as _;
        let mut pixels = self.pixels_mut();
        for i in 0..h {
            let row = &mut pixels[i];
            row.reverse();
        }
    }

    fn to_texture(&self, texture_creator: &'static TextureCreator) -> Texture {
        self.as_texture(texture_creator)
            .expect("unable to render texture")
            .into()
    }

    fn pixels_mut(&mut self) -> SpritePixels {
        SpritePixels::new(self)
    }
}

pub struct SpritePixels<'a> {
    buff: &'a mut [u8],
    width: usize,
    height: usize,
    pitch: usize,
}
impl<'a> SpritePixels<'a> {
    fn new(sprite: &'a mut Surface) -> Self {
        let (width, height) = sprite.size();
        let pitch = sprite.pitch() as _;
        Self {
            buff: sprite.without_lock_mut().expect("Must lock surface"),
            width: width as _,
            height: height as _,
            pitch,
        }
    }

    fn get_row_range(&self, row: usize) -> Range<usize> {
        assert!(row < self.height);
        let start = row * self.pitch;
        let end = start + self.width * size_of::<u32>();
        start..end
    }

    fn swap_rows(&mut self, top_row: usize, bottom_row: usize) {
        let top_row_range = self.get_row_range(top_row);
        let bottom_row_range = self.get_row_range(bottom_row);

        let (left, right) = self.buff.split_at_mut(bottom_row_range.start);

        let top_row = &mut left[top_row_range];
        let bottom_row = &mut right[..bottom_row_range.len()];

        top_row.swap_with_slice(bottom_row);
    }
}
impl Index<usize> for SpritePixels<'_> {
    type Output = [u32];

    fn index(&self, index: usize) -> &Self::Output {
        cast_slice(&self.buff[self.get_row_range(index)])
    }
}
impl IndexMut<usize> for SpritePixels<'_> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        let range = self.get_row_range(index);
        cast_slice_mut(&mut self.buff[range])
    }
}

#[macro_export]
macro_rules! sprite_text {
    ($ss:ident, $tc:ident, $($t:tt)+) => {
        {
            pub use crate::graphics::sprite::_sprite_text;
            _sprite_text($ss, $tc, format_args!($($t)+))
        }
    }
}

pub fn _sprite_text(ss: &Spritesheet, tc: &'static TextureCreator, args: fmt::Arguments) -> Texture {
    ss.render_str(tc, &fmt::format(args))
}

/// A world "ground" material type (wall, ground/dirt, and grass)
#[allow(missing_docs)]
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq, Sequence)]
pub enum Material {
    StoneWall,
    Ground,
    Grass,
}

/// A facing for a [Material]
#[allow(missing_docs)]
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq, Sequence)]
pub enum FaceDirection {
    TopLeft,
    Top,
    TopRight,
    Left,
    Center,
    Right,
    BottomLeft,
    Bottom,
    BottomRight,
}

/// A pairing of a material and a direction
#[allow(missing_docs)]
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq, Sequence)]
pub struct MaterialFace {
    material: Material,
    facing: FaceDirection,
}

/// A value representing a stage in a wheat crop's growth.
///
/// # Panics
/// Panics if the value is outside the range `0..4`.
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub struct CropGrowthStage(usize);
impl CropGrowthStage {
    const NUM_STAGES: usize = 4;
}
impl From<usize> for CropGrowthStage {
    fn from(i: usize) -> Self {
        assert!(
            i < Self::NUM_STAGES,
            "Growth stage must be less than {}",
            Self::NUM_STAGES
        );
        Self(i)
    }
}
impl Sequence for CropGrowthStage {
    const CARDINALITY: usize = Self::NUM_STAGES;

    fn next(&self) -> Option<Self> {
        if self.0 < Self::NUM_STAGES - 1 {
            Some(Self(self.0 + 1))
        } else {
            None
        }
    }

    fn previous(&self) -> Option<Self> {
        (self.0 > 0).then_some(Self(self.0 - 1))
    }

    fn first() -> Option<Self> {
        Some(Self(0))
    }

    fn last() -> Option<Self> {
        Some(Self(Self::NUM_STAGES - 1))
    }
}

/// A valid ASCII character in the sprite.
///
/// # Panics
/// Attempts to use with a character not in the sprite causes a panic.
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub struct SpriteChar(char);
impl SpriteChar {
    pub const ALPHABET: [char; 26 * 2 + 3] =
        to_char_array!(r#"ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789.,!?'"-+=/\%()<>:;"#);

    fn position(&self) -> usize {
        Self::ALPHABET.iter().position(|ch| *ch == self.0).unwrap()
    }
}
impl From<char> for SpriteChar {
    fn from(ch: char) -> Self {
        assert!(ch.is_ascii(), "Sprite character must be ASCII");
        let ch = ch.to_uppercase().next().unwrap();
        assert!(Self::ALPHABET.contains(&ch), "Character not in sprite alphabet");
        Self(ch)
    }
}
impl From<&char> for SpriteChar {
    fn from(ch: &char) -> Self {
        Self::from(*ch)
    }
}
impl Sequence for SpriteChar {
    const CARDINALITY: usize = Self::ALPHABET.len();

    fn next(&self) -> Option<Self> {
        let i = self.position();
        if i != Self::ALPHABET.len() - 1 {
            Some(Self(Self::ALPHABET[i + 1]))
        } else {
            None
        }
    }

    fn previous(&self) -> Option<Self> {
        let i = self.position();
        if i != 0 {
            Some(Self(Self::ALPHABET[i - 1]))
        } else {
            None
        }
    }

    fn first() -> Option<Self> {
        Self::ALPHABET.first().map(Self::from)
    }

    fn last() -> Option<Self> {
        Self::ALPHABET.last().map(Self::from)
    }
}

#[allow(missing_docs)]
#[non_exhaustive]
#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq, Sequence)]
pub enum Sprite {
    Ground1,
    Ground2,
    Ground3,
    Ground4,
    Stone,
    Flower,
    Footprint,
    Edge(MaterialFace), // Stone wall, ground, and grass
    // Some 2x2 sprite blob?
    TreeSingle,
    TreeCluster,
    Sapling,
    // Ground
    // Grass
    // 1x4 ???
    Ore,
    StepsDown,
    StepsUp,
    Crop(Option<CropGrowthStage>),
    Cactus,

    MenuEdge(FaceDirection),

    Chest,

    // ...
    Title,
    Character(SpriteChar),
}
