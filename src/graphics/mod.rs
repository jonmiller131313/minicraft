//! Module handling pixel-based graphics

use sdl2::{
    render::{Texture as SdlTexture, TextureCreator as SdlTextureCreator, TextureQuery},
    video::WindowContext,
};

use self::geometry::SPRITE_SIZE;

pub mod geometry;
pub mod menu;
pub mod noise;
pub mod render;
pub mod sprite;

pub type TextureCreator = SdlTextureCreator<WindowContext>;

pub struct Texture {
    texture: SdlTexture<'static>,
    width: usize,
    height: usize,
    tile_width: usize,
    tile_height: usize,
}
impl Texture {
    pub fn size(&self) -> (usize, usize) {
        (self.width, self.height)
    }
    pub fn tile_size(&self) -> (usize, usize) {
        (self.tile_width, self.tile_height)
    }
}

impl From<SdlTexture<'static>> for Texture {
    fn from(texture: SdlTexture<'static>) -> Self {
        let TextureQuery { width, height, .. } = texture.query();
        Self {
            texture,
            width: width as usize,
            height: height as usize,
            tile_width: width as usize / SPRITE_SIZE,
            tile_height: height as usize / SPRITE_SIZE,
        }
    }
}

impl AsRef<SdlTexture<'static>> for Texture {
    fn as_ref(&self) -> &SdlTexture<'static> {
        &self.texture
    }
}

impl AsMut<SdlTexture<'static>> for Texture {
    fn as_mut(&mut self) -> &mut SdlTexture<'static> {
        &mut self.texture
    }
}
