//! Geometry primitives to convert tile coordinates to raw pixel coordinates.

use sdl2::rect::{Point, Rect};

/// The square size of a single sprite tile.
pub const SPRITE_SIZE: usize = 8;

/// A [Rect] extension trait that facilitates converting to raw pixel coordinates from sprite tile grid coordinates.
///
/// For example, a width/height of 1 is a square with a size of [SPRITE_SIZE].
pub trait RectTileExt {
    /// Initializes a new [Rect] from sprite tile coordinates. Similar signature to [Rect::new()].
    fn new_tile(x: i32, y: i32, w: u32, h: u32) -> Self;

    fn tile_size(&self) -> (u32, u32);
}
impl RectTileExt for Rect {
    fn new_tile(x: i32, y: i32, w: u32, h: u32) -> Self {
        const SCALE_I: i32 = SPRITE_SIZE as i32;
        const SCALE_U: u32 = SPRITE_SIZE as u32;
        Rect::new(x * SCALE_I, y * SCALE_I, w * SCALE_U, h * SCALE_U)
    }

    fn tile_size(&self) -> (u32, u32) {
        const SPRITE_SIZE_U: u32 = SPRITE_SIZE as u32;
        let (_, _, w, h) = (*self).into();
        assert_eq!(
            (w % SPRITE_SIZE_U, h % SPRITE_SIZE_U),
            (0, 0),
            "Rectangle width does not align with texture atlas."
        );

        (w / SPRITE_SIZE_U, h / SPRITE_SIZE_U)
    }
}

/// A [Point] extension trait that facilitates converting to raw pixel coordinates from sprite tile grid coordinates.
pub trait PointTileExt {
    /// Initializes a new [Point] from sprite tile coordinates. Similar signature to [Point::new()].
    fn new_tile(x: i32, y: i32) -> Self;

    fn to_tile(&self) -> (i32, i32);
}
impl PointTileExt for Point {
    fn new_tile(x: i32, y: i32) -> Self {
        let scale_i = SPRITE_SIZE as i32;
        Point::new(x * scale_i, y * scale_i)
    }

    fn to_tile(&self) -> (i32, i32) {
        const SPRITE_SIZE_I: i32 = SPRITE_SIZE as i32;
        let (x, y) = (*self).into();
        assert_eq!(
            (x % SPRITE_SIZE_I, y % SPRITE_SIZE_I),
            (0, 0),
            "Point should be aligned to tile grid."
        );
        (x / SPRITE_SIZE_I, y / SPRITE_SIZE_I)
    }
}
