use std::cell::RefCell;

use anyhow::{Context, Result};
use sdl2::{
    pixels::Color,
    rect::{Point, Rect},
    render::WindowCanvas,
    Sdl,
};

use super::{geometry::SPRITE_SIZE, sprite::Surface, Texture};
use crate::{fps::Fps, util::SdlContextExt};

pub fn init_screen(sdl: &Sdl) -> Result<WindowCanvas> {
    sdl.video()
        .context("unable to initialize SDL video subsystem")?
        .window(env!("CARGO_PKG_NAME"), 854, 480)
        .position_centered()
        .vulkan()
        .build()
        .context("unable to create window")?
        .into_canvas()
        .accelerated()
        .build()
        .context("unable to initialize SDL renderer canvas")
}

#[allow(missing_docs)]
pub struct Renderer<'a> {
    canvas: RefCell<&'a mut WindowCanvas>,
    window_width: usize,
    window_height: usize,
    fps: RefCell<Fps>,
}
impl<'a> Renderer<'a> {
    pub fn init(canvas: &'a mut WindowCanvas) -> Result<Self> {
        let window_scale = 2.0;
        canvas
            .set_scale(window_scale, window_scale)
            .context("unable to set window scale")?;

        let (mut width, mut height) = canvas.output_size().context("unable to get window size")?;
        width /= window_scale as u32;
        height /= window_scale as u32;

        Ok(Self {
            canvas: RefCell::new(canvas),
            window_width: width as _,
            window_height: height as _,
            fps: RefCell::new(Fps::start()),
        })
    }

    pub fn shape(&self) -> (usize, usize) {
        (self.window_width, self.window_height)
    }

    pub fn tile_shape(&self) -> (usize, usize) {
        (self.window_width / SPRITE_SIZE, self.window_height / SPRITE_SIZE)
    }

    pub fn set_window_icon(&self, icon: &Surface) {
        (*self.canvas.borrow_mut()).window_mut().set_icon(icon)
    }

    pub fn fill_screen(&self, color: Color) {
        let mut canvas = self.canvas.borrow_mut();
        canvas.set_draw_color(color);
        canvas.clear();
    }

    pub fn render_texture(&self, texture: &Texture, pos: Point) {
        self.render_texture_ex(texture, pos, 0.0, None, false, false);
    }

    pub fn render_texture_ex(
        &self,
        texture: &Texture,
        pos: Point,
        angle: f64,
        center: impl Into<Option<Point>>,
        flip_h: bool,
        flip_v: bool,
    ) {
        let (x, y) = pos.into();
        let (w, h) = texture.size();
        let dst = Rect::new(x, y, w as u32, h as u32);
        let center = center.into();
        (*self.canvas
            .borrow_mut())
            .copy_ex(texture.as_ref(), None, dst, angle, center, flip_h, flip_v)
            .unwrap_or_else(|e| {
                panic!("unable to render texture (size {w}x{h}) at position {pos:?} (options: angle={angle:.3}, center={center:?}, flip_h={flip_h}, flip_v={flip_v}): {e}")
            });
    }

    pub fn render_texture_centered(&self, texture: &Texture, row: usize) {
        let x = (self.window_width - texture.size().0) / 2;
        self.render_texture(texture, Point::new(x as _, (row * SPRITE_SIZE) as _))
    }

    pub fn render_texture_right(&self, texture: &Texture, row: usize) {
        let x = self.window_width - texture.size().0;
        self.render_texture(texture, Point::new(x as _, (row * SPRITE_SIZE) as _))
    }

    /// Flushes the video buffer to screen. See [`WindowCanvas::present`].
    pub fn present(&self) {
        (*self.canvas.borrow_mut()).present();
        (*self.fps.borrow_mut()).update();
    }

    pub fn fps(&self) -> u32 {
        **self.fps.borrow()
    }
}
