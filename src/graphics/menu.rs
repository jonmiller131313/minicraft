use std::cell::RefCell;

use sdl2::rect::Point;

use super::{geometry::PointTileExt, render::Renderer, sprite::sheet::Spritesheet, Texture, TextureCreator};

pub struct CenteredMenu {
    title: Option<Texture>,
    elements: Vec<RefCell<Texture>>,
    selection: usize,
    padding: usize,
    selection_textures: (Texture, Texture),
}
impl CenteredMenu {
    pub fn new<'a, const N: usize>(
        ss: &Spritesheet,
        tc: &'static TextureCreator,
        title: impl Into<Option<&'a str>>,
        padding: usize,
        elements: [&str; N],
    ) -> Self {
        debug_assert!(!elements.is_empty(), "Static menu must have elements");
        Self {
            title: title.into().map(|t| ss.render_str(tc, t)),
            elements: elements
                .into_iter()
                .map(|e| RefCell::new(ss.render_str(tc, e)))
                .collect(),
            selection: 0,
            padding,
            selection_textures: (ss.render_str(tc, ">"), ss.render_str(tc, "<")),
        }
    }

    pub fn down(&mut self) {
        if self.selection < self.elements.len() - 1 {
            self.selection += 1;
        }
    }

    pub fn up(&mut self) {
        if self.selection != 0 {
            self.selection -= 1;
        }
    }

    pub fn current_selection(&self) -> usize {
        self.selection
    }

    pub fn render(&self, screen: &Renderer, mut row: usize) {
        let (left, right) = &self.selection_textures;

        if let Some(title) = &self.title {
            let title_pos =
                Point::new_tile(0, row as _) + Point::new((screen.shape().0 as i32 - title.width as i32) / 2, 0);
            screen.render_texture(title, title_pos);
            row += 1;
        }

        for (i, elem) in self.elements.iter().enumerate() {
            let mut elem = elem.borrow_mut();
            let mut pos = if i == self.selection {
                let pos = (Point::new(screen.shape().0 as i32 - elem.width as i32, 0)
                    - Point::new_tile(self.padding as i32 * 2 + 2, 0))
                    / 2
                    + Point::new_tile(0, row as _);
                screen.render_texture(left, pos);
                pos + Point::new_tile(1 + self.padding as i32, 0)
            } else {
                elem.as_mut().set_color_mod(0x66, 0x66, 0x66);
                Point::new((screen.shape().0 as i32 - elem.width as i32) / 2, 0) + Point::new_tile(0, row as _)
            };

            screen.render_texture(&elem, pos);

            if i == self.selection {
                pos += Point::new_tile(elem.tile_width as i32 + self.padding as i32, 0);
                screen.render_texture(right, pos);
            }

            elem.as_mut().set_color_mod(0xFF, 0xFF, 0xFF);
            row += 1;
        }
    }
}
