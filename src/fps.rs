//! FPS timer utilities
//!
//! Call [`Fps::update()`] each frame, and read from the [`Fps::value`] field.
use std::{
    ops::Deref,
    time::{Duration, Instant},
};

/// An FPS counter
pub struct Fps {
    value: u32,
    counter: u32,
    next_update: Instant,
}
impl Fps {
    /// Initializes and starts a new counter.
    pub fn start() -> Self {
        Self {
            value: 0,
            counter: 0,
            next_update: Instant::now() + Duration::from_secs(1),
        }
    }

    /// Should be called each frame.
    pub fn update(&mut self) {
        if Instant::now() > self.next_update {
            self.value = self.counter;
            self.counter = 0;
            self.next_update = Instant::now() + Duration::from_secs(1)
        } else {
            self.counter += 1;
        }
    }
}
impl Deref for Fps {
    type Target = u32;

    /// Gets the current value.
    fn deref(&self) -> &Self::Target {
        &self.value
    }
}
