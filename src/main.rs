use anyhow::{Context, Result};
use sdl2::event::{
    Event::{Quit, Window},
    WindowEvent::FocusLost as SdlFocusLost,
};

use crate::{
    events::{Event, EventReader},
    graphics::{render::*, sprite::sheet::Spritesheet},
    screens::{main_menu::MainMenu, window_focus::FocusLost, Scene, SceneTransition},
    util::SdlContextExt,
};

mod events;
mod fps;
mod graphics;
mod screens;
mod util;

fn main() -> Result<()> {
    let sdl = sdl2::init().context("unable to initialize SDL library")?;

    let mut c = init_screen(&sdl)?;

    // Bye bye memory
    let texture_creator = Box::leak(Box::new(c.texture_creator()));
    let spritesheet = Box::leak(Box::new(Spritesheet::load().context("unable to load spritesheet")?));

    let screen = Renderer::init(&mut c)?;
    screen.set_window_icon(&spritesheet.make_window_icon());

    let mut event_reader = EventReader::new(&sdl);
    let mut scenes: Vec<Box<dyn Scene>> = vec![Box::new(MainMenu::new(texture_creator, spritesheet))];
    while !scenes.is_empty() {
        let current_screen = scenes.last_mut().unwrap();
        match event_reader.read_event() {
            Event::UiTick => {
                current_screen.render(&screen);
                screen.present();
            }
            Event::UiInput(Quit { .. }) => break,
            Event::UiInput(Window {
                win_event: SdlFocusLost,
                ..
            }) => scenes.push(Box::new(FocusLost::new(texture_creator, spritesheet))),
            Event::UiInput(event) => match current_screen.tick(event) {
                SceneTransition::Pop => {
                    scenes.pop();
                }
                SceneTransition::Push(new) => {
                    scenes.push(new);
                }
                _ => (),
            },
        }
    }

    Ok(())
}
